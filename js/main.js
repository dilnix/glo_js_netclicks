// elements
const TMDB_URL = 'https://image.tmdb.org/t/p/w185_and_h278_bestv2';
const API_KEY = '50f3777405e44fa3417754e4d06243a2';
const SERVER = 'https://api.themoviedb.org/3';
const LANG = 'ru-RU';
const leftMenu = document.querySelector('.left-menu');
const hamburger = document.querySelector('.hamburger');
const dropdown = document.querySelectorAll('.dropdown');
const tvShowList = document.querySelector('.tv-shows__list');
const modal = document.querySelector('.modal');
const tvShowSec = document.querySelector('.tv-shows');
const tvCardImg = document.querySelector('.tv-card__img');
const modalTitle = document.querySelector('.modal__title');
const firstAir = document.querySelector('.first__air');
const lastAir = document.querySelector('.last__air');
const genresList = document.querySelector('.genres-list');
const rating = document.querySelector('.rating');
const description = document.querySelector('.description');
const modalLink = document.querySelector('.modal__link');
const searchForm = document.querySelector('.search__form');
const searchFormInput = document.querySelector('.search__form-input');
const preloader = document.querySelector('.preloader');
const tvShowsHead = document.querySelector('.tv-shows__head');


// showlist preloader element
const loader = document.createElement('div');
loader.className = 'loading';

// class for getting data from remote DB
const DBRequest = class {
    getData = async (url) => {
        tvShowSec.append(loader); //adding loader before requesting cards
        const res = await fetch(url);
        if (res.ok) {
            return res.json();
        } else {
            throw new Error(`Failed to get data from address ${url}`);
        }
    }

    getTestData = () => {
        return this.getData('test.json');
    }

    getTestCard = () => {
        return this.getData('card.json');
    }

    getSearchResults = query => this.getData(`${SERVER}/search/tv?api_key=${API_KEY}&query=${query}&language=${LANG}&page=1&include_adult=true`);

    getTvShow = id => this.getData(`${SERVER}/tv/${id}?api_key=${API_KEY}&language=${LANG}`);

    getTopRated = () => this.getData(`${SERVER}/tv/top_rated?api_key=${API_KEY}&language=${LANG}&page=1`);

    getPopular = () => this.getData(`${SERVER}/tv/popular?api_key=${API_KEY}&language=${LANG}&page=1`);

    getToday = () => this.getData(`${SERVER}/tv/airing_today?api_key=${API_KEY}&language=${LANG}&page=1`);

    getWeek = () => this.getData(`${SERVER}/tv/on_the_air?api_key=${API_KEY}&language=${LANG}&page=1`);

    getTrending = () => this.getData(`${SERVER}/trending/tv/week?api_key=${API_KEY}`);
}

// testing search request
// console.log(new DBRequest().getSearchResults('няня'));


// card layout rendering
const renderCard = (response, target) => {
    tvShowList.textContent = '';
    if (response.total_results) {response.results.forEach(item => {
        const {
            id,
            backdrop_path: backdrop,
            name: title,
            poster_path: poster,
            vote_average: vote
        } = item;
        const posterIMG = poster ? TMDB_URL + poster : 'img/no-poster.jpg';
        const backdropIMG = backdrop ? TMDB_URL + backdrop : '';
        const voteElem = vote ? `<span class="tv-card__vote">${vote}</span>` : '';
        const card = document.createElement('li');
        card.className = 'tv-shows__item'; // use .classList.add() to append class instead of replace
        card.innerHTML = `
            <a href="#" id=${id} class="tv-card">
                ${voteElem}
                <img class="tv-card__img"
                    src="${posterIMG}"
                    data-backdrop="${backdropIMG}""
                    alt="${title}">
                <h4 class="tv-card__head">${title}</h4>
            </a>
        `;
        loader.remove(); // removing loader before updating cards
        tvShowList.append(card);
    });} else {
        loader.remove(); // removing loader before updating cards
        tvShowsHead.textContent = 'По вашему запросу сериалов не найдено!';
        return;
    }
    tvShowsHead.textContent = target == search ? 'Бьют рейтинги' : target ? target.textContent : 'Результат поиска';
};

// submit catcher and router
searchForm.addEventListener('submit', event => {
    event.preventDefault();
    const value = searchFormInput.value.trim();
    if (value) {
        new DBRequest().getSearchResults(value).then(renderCard);
    }
    searchFormInput.value = '';
});

// opening/closing menu

const removeDropdown = () => {
    dropdown.forEach(item => {
        item.classList.remove('active');
    });
};

hamburger.addEventListener('click', () => {
    leftMenu.classList.toggle('openMenu');
    hamburger.classList.toggle('open');
    removeDropdown();
});

document.addEventListener('click', event => {
    const menuArea = event.target.closest('.left-menu')
    if (!menuArea) {
        leftMenu.classList.remove('openMenu');
        hamburger.classList.remove('open');
        removeDropdown();
    }
});

// dropdown menu items
leftMenu.addEventListener('click', event => {
    const dropdown = event.target.closest('.dropdown');
    if (dropdown) {
        event.preventDefault();
        dropdown.classList.toggle('active');
        leftMenu.classList.add('openMenu');
        hamburger.classList.add('open');
    }
    if (event.target.closest('#top-rated')) {
        new DBRequest().getTopRated().then((response) => renderCard(response, event.target));
    }
    if (event.target.closest('#popular')) {
        new DBRequest().getPopular().then((response) => renderCard(response, event.target));
    }
    if (event.target.closest('#today')) {
        new DBRequest().getToday().then((response) => renderCard(response, event.target));
    }
    if (event.target.closest('#week')) {
        new DBRequest().getWeek().then((response) => renderCard(response, event.target));
    }
    if (event.target.closest('#search')) {
        new DBRequest().getTrending().then((response) => renderCard(response, search));
    }
});

// change card items img src with data-backdrop and back
const changeImage = event => {
    const card = event.target.closest('.tv-shows__item');
    if (card) {
        const cardImg = card.querySelector('.tv-card__img');
        if (cardImg.dataset.backdrop) {
            [cardImg.src, cardImg.dataset.backdrop] = [cardImg.dataset.backdrop, cardImg.src];
        }
    }
};

tvShowList.addEventListener('mouseover', changeImage);
tvShowList.addEventListener('mouseout', changeImage);

// opening/closing of modal window
tvShowList.addEventListener('click', event => {
    event.preventDefault();
    const card = event.target.closest('.tv-card');
    if (card) {
        preloader.style.display = 'block';
        new DBRequest().getTvShow(card.id).then(response => {
            tvCardImg.src = response.poster_path ? TMDB_URL + response.poster_path: 'img/no-poster.jpg';
            tvCardImg.alt = response.name;
            modalTitle.textContent = response.name;
            firstAir.innerHTML = `
                <p class="first__air">Выпущено: ${response.first_air_date}</p>
            `;
            lastAir.innerHTML = `
                <p class="last__air">Последний эпизод: ${response.last_air_date}</p>
            `;
            genresList.textContent = '';
            response.genres.forEach(item => {
                genresList.innerHTML += `<li>${item.name}</li>`;
            });
            rating.textContent = response.vote_average;
            description.textContent = response.overview;
            modalLink.href = response.homepage ? response.homepage : `https://google.com/search?&q=${response.name}`; //to prevent linking home back in case of non exist real homepage
        })
        .then(() => {
            document.body.style.overflow = 'hidden';
            modal.classList.remove('hide');
        })
        .finally(() => {
            preloader.style.display = '';
        })
    }
});

modal.addEventListener('click', event => {
    const cross = event.target.closest('.cross');
    const window = event.target.classList.contains('modal');
    if (cross || window) {
        document.body.style.overflow = '';
        modal.classList.add('hide');
    }
    loader.remove(); // removing loader before updating cards
});

